# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Upcoming]
### Added
- Container with Sphinx, LaTeX and Read the Docs theme.

[Upcoming]: https://gitlab.com/doksi/sphinx/-/compare/200e8a75...master
