FROM sphinxdoc/sphinx-latexpdf

LABEL maintainer="Christoffer Reijer" \
      io.openshift.tags="sphinx,doksi,documentation" \
      io.k8s.description="Document builder for Doksi"

WORKDIR /docs
ADD requirements.txt /docs
RUN /usr/local/bin/python -m pip install --upgrade pip \
  && pip3 install -r requirements.txt
