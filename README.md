# Sphinx

A custom Sphinx container with extra packages installed.

## Using

This image is not meant to be used manually. It is used internally by Doksi when
building documentats during export.

## Building

Build the image like so:

```shell
# with podman
podman build -t sphinx .

# with docker
docker build -t sphinx .
```
